# Global Content

This module allows you to create global content items which will be
displayed on the whole website (ie. header, footer, block, etc.).
By default, this module uses the "block" and "page" theme pre-process
hooks (see ".module" file).


## Contents of this file

- Installation
- Configuration
- Maintainers

## Installation

Install the IEF Complex Open Widget module as you would normally install a
contributed Drupal module. Visit [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-modules)
for further information.

## Configuration

Here are steps to configure and display global content items:

- Go to "Content" > "Global Content" > "Global Content Settings"
- Go to "Manage fields" and add fields
- Go to "Content" > "Global Content" and click on "Add one" link

Open the twig files corresponding to the place you want to display
your global content items.

Insert `{{ global_content_MY_FIELD_NAME }}` where "MY_FIELD_NAME" is
your field's machine name without the drupal "field_" prefix.

## Maintainers

- George Anderson - [geoanders](https://www.drupal.org/u/geoanders)
- Shashank Kumar - [shashank5563](https://www.drupal.org/u/shashank5563)