<?php

/**
 * @file
 * Contains global_content.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function global_content_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the global_content module.
    case 'help.page.global_content':
      $output = '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('This module allows you to create global content items which will be displayed on the whole website (ie. header, footer, block, etc.). By default, this module uses the "block" and "page" theme pre-process hooks (see ".module" file).') . '</p>';
      $output .= '<h3>' . t('Uses') . '</h3>';
      $output .= '<p>' . t('Here are steps to configure and display global content items:') . '</p>';
      $output .= '<ol><li>' . t('Go to "Content" > "Global Content" > "Global Content Settings"') . '</li>';
      $output .= '<li>' . t('Go to "Manage fields" and add fields') . '</li>';
      $output .= '<li>' . t('Go to "Content" > "Global Content" and click on "Add one" link') . '</li>';
      $output .= '<li>' . t('Open the twig files corresponding to the place you want to display your global content items. Insert {{ global_content_MY_FIELD_NAME }} where "MY_FIELD_NAME" is your field\'s machine name without the drupal "field_" prefix.') . '</li></ol>';
      return $output;

    default:
  }
}

/**
 * Implements hook_preprocess_page().
 */
function global_content_preprocess_page(array &$variables) {
  // Create global content variables.
  $global_content_variables = global_content_create_global_variables();

  // Add global content variables to global variables.
  $variables = array_merge($variables, $global_content_variables);
}

/**
 * Implements hook_preprocess_block().
 */
function global_content_preprocess_block(array &$variables) {
  // Create global content variables.
  $global_content_variables = global_content_create_global_variables();

  // Add global content variables to global variables.
  $variables = array_merge($variables, $global_content_variables);
}

/**
 * Create global variables.
 */
function global_content_create_global_variables(): array {
  $global_content_variables = [];
  $entityTypeManager = \Drupal::entityTypeManager();

  /** @var \Drupal\Core\Entity\EntityRepository $entityRepository */
  $entityRepository = \Drupal::service('entity.repository');

  // Load the values.
  if ($global_content_entities = $entityTypeManager->getStorage('global_content')->loadMultiple()) {

    // Get only the first one.
    $global_content = reset($global_content_entities);

    // Get translated values depending on the context.
    $translation = $entityRepository->getTranslationFromContext($global_content);

    // Loop on all fields.
    foreach ($translation->getFieldDefinitions() as $key => $field) {

      // Only custom fields created via UI.
      if (preg_match('/^field\_/i', $key)) {

        // Variable renaming in order to easily identify GC variables in TWIG.
        $global_content_var_name = preg_replace('/^field\_/i', 'global_content_', $key);

        // Add a global variable.
        $global_content_variables[$global_content_var_name] = $translation->get($key)->getValue();
      }
    }
  }

  return $global_content_variables;
}
