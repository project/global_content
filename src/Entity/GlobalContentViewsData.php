<?php

namespace Drupal\global_content\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Global Content entities.
 */
class GlobalContentViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();
    return $data;
  }

}
